#!/usr/bin/python3

import time
import random
import argparse
import matplotlib.pyplot as plt
from operator import itemgetter

ap = argparse.ArgumentParser()
ap.add_argument("-g", "--generations", type=int, required=True,
    help="Number of generation to be executed")
args = vars(ap.parse_args())


# Global variables
newGeneration = []
populationSize = 100
bestRoutes = []


def getPositions():
    positionsFile = open("positions.txt", 'r')
    positions = []

    for x in positionsFile:
        line = x.replace('\n','')
        line = line.replace(' ', '')
        coordinates = list(line)

        positions.append(coordinates)

    return positions

def createPopulation(cromosomas):
    population = []

    for x in range(0, cromosomas):
        genes = list( range(1, 21) )
        random.shuffle(genes)
        population.append(genes)
    
    return population

def calculateRouteDistance(route, positions):
    totalDistance = 0

    for x in range(0, 19):
        city1 = route[x] - 1
        city2 = route[x + 1] - 1 
    
        totalDistance += pow ( pow( ( int(positions[city2][0]) - int(positions[city1][0]) ), 2) + pow( ( int(positions[city2][1]) - int(positions[city1][1]) ), 2), 0.5)

    return totalDistance

def createGraph(route, positions):
    x = []
    y = []

    for city in route[0:20]:
        x.append( int(positions[city - 1][0]) )
        y.append( int(positions[city - 1][1]) )

    plt.plot(x, y, color='green', linestyle='dashed', linewidth = 3,
             marker='o', markerfacecolor='blue', markersize=12)

    plt.xlim(0,10)
    plt.ylim(0,9)

    plt.xlabel('x - axis')
    plt.ylabel('y - axis')

    plt.title('Route')

    plt.draw()
    plt.pause(0.002)
    plt.clf()

def changingBlocks(cromosome):
    amount = random.randrange(1, 10)
    pos1 = random.randrange(0, (10 - amount))
    pos2 = random.randrange((pos1 + amount + 1), (19 - amount + 1)) # check it

    for x in range(0, amount):
        frame = cromosome[pos1 + x]
        cromosome[pos1 + x] = cromosome[pos2 + x]
        cromosome[pos2 + x] = frame

    return cromosome

def reverseBlock(cromosome):
    amount = random.randrange(2, 19)
    pos = random.randrange(0, (20 - amount + 1))

    reversedBlock = cromosome[pos:(pos + amount):]
    reversedBlock.reverse()

    for x in range(0, amount):
        cromosome[pos + x] = reversedBlock[x]
    
    return cromosome

def tournament(population):
    players = list(range(0,20))
    random.shuffle(players)
    participants = players[8:13]

    participants.sort()

    winner = participants.pop(0)
    cromosome = population[winner]
    
    coin = random.randrange(0,2)
    
    if(coin):
        x = changingBlocks(cromosome[0:20])
    else:
        x = reverseBlock(cromosome[0:20])
    newGeneration.append(x)

def calculateDistanceOfPopulation(population):
    for x in range(0,populationSize):
        distance = calculateRouteDistance(listA[x], positions)
        listA[x].append(distance)
    
    population.sort(key=itemgetter(-1))

def generateNewGeneration(population, positions):
    for gen in range(0, populationSize):
        tournament(listA)
    
    population[:] = []

    for x in range(0, populationSize):
        population.append(newGeneration[x])

    newGeneration[:] = []
    calculateDistanceOfPopulation(population)
    bestRoutes.append(population[0][-1])

    createGraph(population[0], positions)

def createDistanceGraph(population):
    x1 = []
    y1 = []

    for city in population[0:20]:
        x1.append( int(positions[city - 1][0]) )
        y1.append( int(positions[city - 1][1]) )
    
    plt.subplot(2, 1, 1)
    plt.plot(x1, y1, color='green', linestyle='dashed', linewidth = 3,
             marker='o', markerfacecolor='blue', markersize=12)

    plt.xlim(0,10)
    plt.ylim(0,9)

    plt.xlabel('x - axis')
    plt.ylabel('y - axis')

    plt.title('Route')
    
    x2 = list(range(1, int(args["generations"] + 1)))

    plt.subplot(2, 1, 2)
    plt.plot(x2, bestRoutes, color='green', linestyle='dashed', linewidth = 1,
             marker='o', markerfacecolor='blue', markersize=5)

    plt.xlim(0,100)
    plt.ylim(20,80)

    plt.xlabel('Generations')
    plt.ylabel('Distance')

    plt.title('Distance')
    #plt.show()



positions = getPositions()
listA = createPopulation(populationSize)
calculateDistanceOfPopulation(listA)

for x in range(0,args["generations"]):
    generateNewGeneration(listA, positions)

print("Route")
print(listA[0][0:20])
print("Distance")
print(listA[0][-1])

createDistanceGraph(listA[0])
plt.show()
