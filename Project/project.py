#!/usr/bin/python3

import math
import random
import argparse
import itertools
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from operator import itemgetter

ap = argparse.ArgumentParser()
ap.add_argument("-g", "--generations", type=int, required=True,
            help="Number of generation to be executed")
args = vars(ap.parse_args())

target = [[7,7,8,7,8,8], [8,8,8,7,8,8], [8,8,7,8,7,8], [8,8,7,7,8,7], [7,7,8,8,7,7], [7,8,8,7,7,8], [7,7,8,7,8,8],
          [7,7,7,8,8,8], [7,8,7,8,8,8], [7,8,8,7,8,7], [8,8,7,7,7,8], [7,7,7,8,8,7], [8,7,8,7,7,8]]

days = np.arange(1,7,1)
teachers = np.arange(1,14,1)

days, teachers = np.meshgrid(days,teachers)

targetGrid = np.array(target)
axisZ = []

# Global Variables
newGeneration = []
bestCoeficients = []
populationSize = 600
tournamentSize = 15
numberOfCoeficients = 27
mx = [2,5,8,2,5,8]

fig = plt.figure(1)
plt1 = fig.add_subplot(221)
plt2 = fig.add_subplot(222)
ax = fig.add_subplot(1, 2, 1, projection='3d')
ax1 = fig.add_subplot(1, 2, 2, projection='3d')

def createPopulation():
    population = []

    for x in range(populationSize):
        cromosome = []

        for y in range(numberOfCoeficients):
            cromosome.append(random.randrange(0,256))

        population.append(cromosome)

    return population

def binaryConversion(number):
    binary = bin(number).replace("0b", "").zfill(8)
    return list(binary)
    
def generateOperand(cromosome):
    operand = []

    for x in range(numberOfCoeficients):
        operand.append(binaryConversion(int(cromosome[x])))

    return operand

def bitChanging(data1, data2, position):
    tmp = []

    tmp = data1[position:8]
    data1[position:8] = data2[position:8]
    data2[position:8] = tmp

def binIntConvertion(data):
    integer = "".join(data)
    binHeader = "0b"
    binHeader += integer

    return int(binHeader,2)

def digitChange(data1, data2):
    tmp = data1
    data1 = data2
    data2 = tmp

    return data1, data2

def crossOperation(data1, data2):
    newCromosome1 = []
    newCromosome2 = []

    position = random.randrange(1,215)
    pivote = int(position / 8)
    bytePosition = position - (pivote * 8)

    bitChanging(data1[pivote],data2[pivote], bytePosition)

    for z in range(pivote):
        data1[z], data2[z] = digitChange(data1[z],data2[z])

    for x in range(numberOfCoeficients):
        newCromosome1.append(binIntConvertion(data1[x]))
        newCromosome2.append(binIntConvertion(data2[x]))

    return newCromosome1, newCromosome2
    
def mutation(population):
    players = list(range(populationSize))
    random.shuffle(players)
    mutants = players[0:tournamentSize]

    for x in mutants:
        mutant = generateOperand(population[x][:27])
        position = random.randrange(216)
        mutantByte = int(position / 8)
        mutantBytePosition = position - (mutantByte * 8)

        selectedBit = mutant[mutantByte][mutantBytePosition]

        mutant[mutantByte][mutantBytePosition] = str( int(selectedBit) ^ 1 )

        for i in range(27):
            population[x][i] = binIntConvertion(mutant[i])

def tournament(population):
    mutation(population)

    players = list(range(populationSize))
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner1 = participants.pop(0)

    # removing first winner in order to get the same winner
    players.remove(winner1)
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner2 = participants.pop(0)

    # converting to binary
    parent1 = generateOperand(population[winner1]) 
    parent2 = generateOperand(population[winner2]) 

    # getting two children for next generation
    child1, child2 = crossOperation(parent1, parent2)

    newGeneration.append(child1)
    newGeneration.append(child2)


def generateNewGeneration(population):
    for gen in range(0,( int(populationSize/2)) ):
        tournament(population)
    
    population[:] = []

    for x in range(0, populationSize):
        population.append(newGeneration[x])

    newGeneration[:] = []

def calculateError(result):
    sumOfRows = []
    error = 0
	
    for x in range(13):
        partialResult = 0

        for y in range(6):
            partialResult += math.fabs(target[x][y] - result[x][y])
		
        sumOfRows.append(partialResult)
	
    for i in sumOfRows:
        error += i

    return error

def neuralNetwork(population):
    for cromosome in population:
        px = []
        qx = []
        rx = []

        for i in range(9):    
            px.append(cromosome[i] / 50)
            qx.append(cromosome[9 + i] / 50)
            rx.append(cromosome[18 + i] / 50)
            
        Z = [[0 for x in range(6)] for y in range(13)]	
        
        # Neural Network
        for x in range(13):

            w1 = math.exp(-(x - mx[0])**2 / (2 * 0.2**2))
            w2 = math.exp(-(x - mx[1])**2 / (2 * 0.2**2))
            w3 = math.exp(-(x - mx[2])**2 / (2 * 0.2**2))

            tmpArray = []

            for y in range(6):

                w4 = math.exp(-(y - mx[3])**2 / (2 * 0.5**2))
                w5 = math.exp(-(y - mx[4])**2 / (2 * 0.5**2))
                w6 = math.exp(-(y - mx[5])**2 / (2 * 0.5**2))

                # Multiplied function
                LowLow = w1 * w4
                LowMed = w1 * w5
                LowHig = w1 * w6

                MedLow = w2 * w4
                MedMed = w2 * w5
                MedHig = w2 * w6

                HigLow = w3 * w4
                HigMed = w3 * w5
                HigHig = w3 * w6

                b = LowLow + LowMed + LowHig + MedLow + MedMed + MedHig + HigLow + HigMed + HigHig

                func1 = ((px[0] * x) + (qx[0] * y) + rx[0]) * LowLow
                func2 = ((px[1] * x) + (qx[1] * y) + rx[1]) * LowMed
                func3 = ((px[2] * x) + (qx[2] * y) + rx[2]) * LowHig
                func4 = ((px[3] * x) + (qx[3] * y) + rx[3]) * MedLow
                func5 = ((px[4] * x) + (qx[4] * y) + rx[4]) * MedMed
                func6 = ((px[5] * x) + (qx[5] * y) + rx[5]) * MedHig
                func7 = ((px[6] * x) + (qx[6] * y) + rx[6]) * HigLow
                func8 = ((px[7] * x) + (qx[7] * y) + rx[7]) * HigMed
                func9 = ((px[8] * x) + (qx[8] * y) + rx[8]) * HigHig

                a = func1 + func2 + func3 + func4 + func5 + func6 + func7 + func8 + func9

                Z[x][y] = a / b

        cromosome.append(Z)
        cromosome.append(calculateError(Z))

population = createPopulation()

for i in range(int(args["generations"])):
    neuralNetwork(population)
    population.sort(key=itemgetter(-1))

    bestCoeficients.append(population[0])
    generateNewGeneration(population)

gen = 0
for y in bestCoeficients:
    gen += 1
    print("Error in generation ", gen, ": ", y[-1])

Z = []
Z = bestCoeficients[-1][-2]
axisZ = np.array(Z)
surf = ax.plot_surface(teachers, days, targetGrid, cmap=cm.Blues, linewidth=0, antialiased=False)
surf1 = ax1.plot_surface(teachers, days, axisZ, cmap=cm.Blues, linewidth=0, antialiased=False)

ax.set_zlim(4,10)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

# Graph for Aptitude Function
y = []
x = list(range(1, int(args["generations"] + 1)))

for z in bestCoeficients:
    y.append(int(z[-1]))

plt.figure(2)
plt.plot(x, y, color='green', linestyle='dashed', linewidth = 1,
         marker='o', markerfacecolor='blue', markersize=5)

plt.xlim(0, int(args["generations"]))
plt.ylim(0,500)

plt.xlabel('Generations')
plt.ylabel('Error')
plt.title('Funcion de Aptitud')

plt.show()
