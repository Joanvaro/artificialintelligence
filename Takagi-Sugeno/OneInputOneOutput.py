#!/usr/bin/python3

import math
import matplotlib.pyplot as plt

m1 = 30
m2 = 60
m3 = 90

p1 = 2.5
p2 = -0.15
p3 = 0.75

q1 = 0
q2 = 15
q3 = 10

axisX =[]
axisY =[]
graph1 = []
graph2 = []
graph3 = []

for i in range(1,1001):
    x = i / 10
    axisX.append(x)

    w1 = math.exp(-(x - m1)**2 / (2 * 12**2))
    w2 = math.exp(-(x - m2)**2 / (2 * 12**2))
    w3 = math.exp(-(x - m3)**2 / (2 * 12**2))

    graph1.append(w1)
    graph2.append(w2)
    graph3.append(w3)
    
    b = w1 + w2 + w3

    r1 = w1 * (p1 * x + q1)
    r2 = w2 * (p2 * x + q2)
    r3 = w3 * (p3 * x + q3)

    a = r1 + r2 + r3

    axisY.append(a/b)

plt.figure(1)
plt.plot(axisX, graph1)
plt.plot(axisX, graph2)
plt.plot(axisX, graph3)

plt.xlim(0,100)
plt.ylim(0,1.2)

plt.xlabel('Speed')
plt.ylabel('Membership')

plt.title('Fuzzy Logic')

plt.figure(2)
plt.plot(axisX, axisY)
plt.xlim(0,100)
plt.ylim(0,100)

plt.xlabel('Speed')
plt.ylabel('Membership')

plt.title('Fuzzy Logic')

#plt.draw()
#plt.pause(0.002)
#plt.clf()
plt.show()
