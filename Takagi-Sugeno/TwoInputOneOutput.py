#!/usr/bin/python3

import math
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

mx = [3,6,9,3,6,9]
px = [0, 0, 0, 0, 1, 0, 0, 0, 0]
qx = [0, 0, 0, 0, 1, 0, 0, 0, 0]
rx = [0, 0, 0, 0, 0, 0, 0, 0, 0]

fig = plt.figure()
ax = fig.gca(projection='3d')

axisX = np.arange(0,10,0.1) 
axisY = np.arange(0,10,0.1)
Z = [[0 for x in range(100)] for y in range(100)]

axisX, axisY = np.meshgrid(axisX, axisY)

for i in range(100):
    x = i/10

    w1 = math.exp(-(x - mx[0])**2 / (2 * 0.5**2))
    w2 = math.exp(-(x - mx[1])**2 / (2 * 0.5**2))
    w3 = math.exp(-(x - mx[2])**2 / (2 * 0.5**2))

    tmpArray = []

    for j in range(100):
        y = j/10

        w4 = math.exp(-(y - mx[3])**2 / (2 * 0.5**2))
        w5 = math.exp(-(y - mx[4])**2 / (2 * 0.5**2))
        w6 = math.exp(-(y - mx[5])**2 / (2 * 0.5**2))

        # Multiplied function
        LowLow = w1 * w4
        LowMed = w1 * w5
        LowHig = w1 * w6

        MedLow = w2 * w4
        MedMed = w2 * w5
        MedHig = w2 * w6

        HigLow = w3 * w4
        HigMed = w3 * w5
        HigHig = w3 * w6

        b = LowLow + LowMed + LowHig + MedLow + MedMed + MedHig + HigLow + HigMed + HigHig

        func1 = ((px[0] * x) + (qx[0] * y) + rx[0]) * LowLow
        func2 = ((px[1] * x) + (qx[1] * y) + rx[1]) * LowMed
        func3 = ((px[2] * x) + (qx[2] * y) + rx[2]) * LowHig
        func4 = ((px[3] * x) + (qx[3] * y) + rx[3]) * MedLow
        func5 = ((px[4] * x) + (qx[4] * y) + rx[4]) * MedMed
        func6 = ((px[5] * x) + (qx[5] * y) + rx[5]) * MedHig
        func7 = ((px[6] * x) + (qx[6] * y) + rx[6]) * HigLow
        func8 = ((px[7] * x) + (qx[7] * y) + rx[7]) * HigMed
        func9 = ((px[8] * x) + (qx[8] * y) + rx[8]) * HigHig

        a = func1 + func2 + func3 + func4 + func5 + func6 + func7 + func8 + func9

        Z[i][j] = a / b


axisZ = np.array(Z)

surf = ax.plot_surface(axisX, axisY, axisZ, cmap=cm.coolwarm, linewidth=0, antialiased=False)

ax.set_zlim(0, 20)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()

