#!/usr/bin/python3

import math
import random
import argparse
import itertools
import matplotlib.pyplot as plt

from operator import itemgetter

ap = argparse.ArgumentParser()
ap.add_argument("-g", "--generations", type=int, required=True,
    help="Number of generation to be executed")
args = vars(ap.parse_args())

# Global Variables
newGeneration = []
bestCoeficients = []
populationSize = 300
tournamentSize = 5
numberOfCoeficients = 27

def createPopulation():
    population = []

    for x in range(populationSize):
        cromosome = []

        for y in range(numberOfCoeficients):
            cromosome.append(random.randrange(0,256))

        population.append(cromosome)

    return population

def binaryConversion(number):
    binary = bin(number).replace("0b", "").zfill(8)
    return list(binary)
    
def generateOperand(cromosome):
    operand = []

    for x in range(numberOfCoeficients):
        operand.append(binaryConversion(int(cromosome[x])))

    return operand

def bitChanging(data1, data2, position):
    tmp = []

    tmp = data1[position:8]
    data1[position:8] = data2[position:8]
    data2[position:8] = tmp

def binIntConvertion(data):
    integer = "".join(data)
    binHeader = "0b"
    binHeader += integer

    return int(binHeader,2)

def digitChange(data1, data2):
    tmp = data1
    data1 = data2
    data2 = tmp

    return data1, data2

def crossOperation(data1, data2):
    newCromosome1 = []
    newCromosome2 = []

    position = random.randrange(1,215)
    pivote = int(position / 8)
    bytePosition = position - (pivote * 8)

    bitChanging(data1[pivote],data2[pivote], bytePosition)

    for z in range(pivote):
        data1[z], data2[z] = digitChange(data1[z],data2[z])

    for x in range(numberOfCoeficients):
        newCromosome1.append(binIntConvertion(data1[x]))
        newCromosome2.append(binIntConvertion(data2[x]))

    return newCromosome1, newCromosome2

def calculateError(operand):
    sumOfCoeficients = 0

    for x in range(numberOfCoeficients):
        sumOfCoeficients += operand[x]

    return math.fabs( 3123 - sumOfCoeficients )

def fillError(population):
    for x in population:
        x.append(calculateError(x))

    # sorting the population by the error
    population.sort(key=itemgetter(-1))


def tournament(population):
    players = list(range(0,populationSize))
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner1 = participants.pop(0)

    # removing first winner in order to get the same winner
    players.remove(winner1)
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner2 = participants.pop(0)

    # converting to binary
    parent1 = generateOperand(population[winner1]) 
    parent2 = generateOperand(population[winner2]) 

    # getting two children for next generation
    child1, child2 = crossOperation(parent1, parent2)

    newGeneration.append(child1)
    newGeneration.append(child2)


def generateNewGeneration(population):
    for gen in range(0,( int(populationSize/2)) ):
        tournament(population)
    
    population[:] = []

    for x in range(0, populationSize):
        population.append(newGeneration[x])

    newGeneration[:] = []

    fillError(population)
    bestCoeficients.append(population[0])


def createErrorGraph():
    y = []

    x = list(range(1, int(args["generations"] + 1)))

    for z in bestCoeficients:
        y.append(int(z[-1]))

    plt.plot(x, y, color='green', linestyle='dashed', linewidth = 1,
             marker='o', markerfacecolor='blue', markersize=5)

    plt.xlim(0,100)
    plt.ylim(0,60)

    plt.xlabel('Generations')
    plt.ylabel('Error')
    plt.title('Funcion de Aptitud')

    plt.show()

population = createPopulation()
fillError(population)

for x in range(0,args["generations"]):
    generateNewGeneration(population)

for x in bestCoeficients:
        print(x)

createErrorGraph()
