#!/usr/bin/python3

import math
import random
import argparse
import itertools
import matplotlib.pyplot as plt

from operator import itemgetter

ap = argparse.ArgumentParser()
ap.add_argument("-g", "--generations", type=int, required=True,
    help="Number of generation to be executed")
args = vars(ap.parse_args())

# Global Variables
newGeneration = []
bestCoeficients = []
populationSize = 100
tournamentSize = 5

def createPopulation():
    population = []

    for x in range(0,populationSize):
        cromosome = []

        cromosome.append(random.randrange(0,256))
        cromosome.append(random.randrange(0,256))
        cromosome.append(random.randrange(0,256))

        population.append(cromosome)

    return population

def binaryConversion(number):
    binary = bin(number).replace("0b", "").zfill(8)
    return list(binary)
    
def generateOperand(cromosome):
    operand = []

    for x in range(0,3):
        operand.append(binaryConversion(int(cromosome[x])))

    return operand

def bitChanging(data1, data2, position):
    tmp = []

    tmp = data1[position:8]
    data1[position:8] = data2[position:8]
    data2[position:8] = tmp

def binIntConvertion(data):
    integer = "".join(data)
    binHeader = "0b"
    binHeader += integer

    return int(binHeader,2)

def digitChange(data1, data2):
    tmp = data1
    data1 = data2
    data2 = tmp

    return data1, data2

def crossOperation(data1, data2):
    newCromosome1 = []
    newCromosome2 = []

    position = random.randrange(1,23)

    if position < 8:
        bitChanging(data1[0],data2[0], position)
        data1[1], data2[1] = digitChange(data1[1],data2[1])
        data1[2], data2[2] = digitChange(data1[2],data2[2])
    elif 8 <= position < 16:
        position -= 8
        bitChanging(data1[1],data2[1], position)
        data1[2], data2[2] = digitChange(data1[2],data2[2])
    else:
        position -= 16
        bitChanging(data1[2],data2[2], position)

    for x in range(0,3):
        newCromosome1.append(binIntConvertion(data1[x]))
        newCromosome2.append(binIntConvertion(data2[x]))

    return newCromosome1, newCromosome2

def calculateError(operand):
    return math.fabs( 13 - (operand[0] + operand[1] + operand[2]) )

def fillError(population):
    for x in population:
        x.append(calculateError(x))

    # sorting the population by the error
    population.sort(key=itemgetter(-1))


def tournament(population):
    players = list(range(0,populationSize))
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner1 = participants.pop(0)

    # removing first winner in order to get the same winner
    players.remove(winner1)
    random.shuffle(players)
    participants = players[0:tournamentSize]

    participants.sort()

    winner2 = participants.pop(0)

    # converting to binary
    parent1 = generateOperand(population[winner1]) 
    parent2 = generateOperand(population[winner2]) 

    # getting two children for next generation
    child1, child2 = crossOperation(parent1, parent2)

    newGeneration.append(child1)
    newGeneration.append(child2)


def generateNewGeneration(population):
    for gen in range(0,( int(populationSize/2)) ):
        tournament(population)
    
    population[:] = []

    for x in range(0, populationSize):
        population.append(newGeneration[x])

    newGeneration[:] = []

    fillError(population)
    bestCoeficients.append(population[0])


def createErrorGraph():
    y = []

    x = list(range(1, int(args["generations"] + 1)))

    for z in bestCoeficients:
        y.append(int(z[3]))

    plt.plot(x, y, color='green', linestyle='dashed', linewidth = 1,
             marker='o', markerfacecolor='blue', markersize=5)

    plt.xlim(0,100)
    plt.ylim(0,60)

    plt.xlabel('Generations')
    plt.ylabel('Error')
    plt.title('Funcion de Aptitud')

    plt.show()

population = createPopulation()
fillError(population)
#population1 = fillError(population)

#for x in population:
#        print(x)

for x in range(0,args["generations"]):
    generateNewGeneration(population)

'''
print(population[0])
print(population[1])
lista1 = generateOperand(population[0])
lista2 = generateOperand(population[1])
print(lista1)
print(lista2)
x,y = crossOperation(lista1, lista2)
print(x)
print(y)
'''

for x in bestCoeficients:
        print(x)

createErrorGraph()
